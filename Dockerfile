FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["s2s-ef.csproj", "./"]
RUN dotnet restore "s2s-ef.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet tool install --global dotnet-ef --version 5.0.0
ENV PATH="${PATH}:/root/.dotnet/tools" 
RUN dotnet-ef
RUN dotnet user-secrets set ConnectionStrings.OneDATDatabase "Server=tcp:onedat-devsqlsrvr.database.windows.net,1433;Initial Catalog=ef-sample;Persist Security Info=False;User ID=ef_user;Password=Str0ngP@ssw0rd;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
RUN dotnet-ef dbcontext scaffold Name=ConnectionStrings.OneDATDatabase Microsoft.EntityFrameworkCore.SqlServer -f -o Models

RUN dotnet build "s2s-ef.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "s2s-ef.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "s2s-ef.dll"]
