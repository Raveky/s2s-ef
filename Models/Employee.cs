﻿using System;
using System.Collections.Generic;

#nullable disable

namespace s2s_ef.Models
{
    public partial class Employee
    {
        public int EmployeesId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
        public int? Salary { get; set; }
    }
}
