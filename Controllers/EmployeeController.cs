using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using s2s_ef.Models;
using s2s_ef.Repository;

namespace s2s_ef.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class EmployeeController : ControllerBase
  {
      private static efsampleContext efContext = new efsampleContext();
      private IRepository<Employee> _repository = new Repository<Employee>(efContext);

    // GET: api/<EmployeeController>
    [Route("/employees")]  
    [HttpGet] 
    public IEnumerable<Employee> Get()
    {
      return _repository.GetAll();
    }

    // GET api/<EmployeeController>/5
    [HttpGet("{id}")]
    public string Get(int id)
    {
      return "value";
    }

    // POST api/<EmployeeController>
    [HttpPost]
    public void Post([FromBody] string value)
    {
    }

    // PUT api/<EmployeeController>/5
    [HttpPut("{id}")]
    public void Put(int id, [FromBody] string value)
    {
    }

    // DELETE api/<EmployeeController>/5
    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
  }
    
}